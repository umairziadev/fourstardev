<?php

namespace Rokanthemes\WidgetSubCategory\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Category extends AbstractHelper
{


    /**
     * @return array
     */
    public function getAdditionalImageTypes()
    {
        return array('thumbnail');
    }

    /**
     * Retrieve image URL
     * @param $image
     * @return string
     */
    public function getImageUrl($image)
    {
        $url = false;
        //$image = $this->getImage();
        if ($image) {
            if (is_string($image)) {
                $url = $this->_urlBuilder->getBaseUrl(
                        ['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]
                    ) . 'catalog/category/' . $image;
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }
        return $url;
    }

    public function getCatagoryImageUrl($img)
    {
        if($img)
        {
            $trim_pub = explode("category/", $img);
            $url_fixed = $img;
            if(is_array($trim_pub) && count($trim_pub) > 0){
                $path_media = ltrim(end($trim_pub), '/');
                $mediaUrl = $this->_urlBuilder->getBaseUrl(
                    ['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]
                );
                $url_fixed = $mediaUrl.'catalog/category/'.$path_media;
            }
            
            return $url_fixed;
        }
        return $img;
    }


}
