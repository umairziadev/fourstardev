<?php
namespace Rokanthemes\Themeoption\Model\Config;

class Footertype implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'footer_furniture_01', 'label' => __('Furniture 01')],
            ['value' => 'footer_furniture_02', 'label' => __('Furniture 02')],
            ['value' => 'footer_furniture_03', 'label' => __('Furniture 03')],
            ['value' => 'footer_furniture_04', 'label' => __('Furniture 04')],
			['value' => 'footer_jewelry_01', 'label' => __('Jewelry_01')],
			['value' => 'footer_jewelry_02', 'label' => __('Jewelry_02')], 
			['value' => 'footer_skincare_01', 'label' => __('Skincare_01')],
			['value' => 'footer_skincare_02', 'label' => __('Skincare_02')], 
			['value' => 'footer_plant_01', 'label' => __('Plant_01')],
			['value' => 'footer_plant_02', 'label' => __('Plant_02')], 
			['value' => 'footer_carparts_01', 'label' => __('Carparts_01')],
			['value' => 'footer_fashion_01', 'label' => __('Fashion_01')],
			['value' => 'footer_fashion_02', 'label' => __('Fashion_02')],
			['value' => 'footer_organic_01', 'label' => __('Organic_01')],
        ];
    }

    public function toArray()
    {
        return [
            'footer_furniture_01' => __('Furniture 01'),
            'footer_furniture_02' => __('Furniture 02'),
            'footer_furniture_03' => __('Furniture 03'),
            'footer_furniture_04' => __('Furniture 04'),
			'footer_jewelry_01' => __('Jewelry_01'),
            'footer_jewelry_02' => __('Jewelry_02'),  
			'footer_skincare_01' => __('Skincare_01'),
            'footer_skincare_02' => __('Skincare_02'),
			'footer_plant_01' => __('Plant_01'),
            'footer_plant_02' => __('Plant_02'), 
			'footer_carparts_01' => __('Carparts_01'),
			'footer_fashion_01' => __('Fashion_01'),
            'footer_fashion_02' => __('Fashion_02'), 
			'footer_organic_01' => __('Organic_01'),
        ];
    }
}